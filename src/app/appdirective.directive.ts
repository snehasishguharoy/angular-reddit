import { Directive, ElementRef,HostListener, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[appAppdirective]'
})
export class AppdirectiveDirective {
  @HostBinding('style.border')border:string;

  constructor(private element:ElementRef,private renderer:Renderer) {
    this.changeColor('green');
   }
  @HostListener('click') onMouseLeave() {
    this.changeColor('red');
    this.border="20px solid brown";
  }
  changeColor(str:string){
    this.renderer.setElementStyle(this.element.nativeElement,'color',str);
  }

}
