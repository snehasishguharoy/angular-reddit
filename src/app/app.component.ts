import { Component } from '@angular/core';
import { Article } from './models/Article';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  articles: Article[];
  constructor() {
    this.articles = [
      new Article("Angular 7", "http://angular.io", 18),
      new Article("Google", "http://www.google.com", 23),
      new Article("Yahoo", "http://www.yahoo.com", 34)
    ]
  }



  addArticle(title: HTMLInputElement, link: HTMLInputElement): boolean {
    console.log("Adding article title: ${title.value} and link: ${link.value}");
    this.articles.push(new Article(title.value, link.value, 0));
    title.value = '';
    link.value = '';
    return false;

  }
  sortedArticles(): Article[]{
  return this.articles.sort((a: Article, b: Article) => b.votes - a.votes);
}

}
